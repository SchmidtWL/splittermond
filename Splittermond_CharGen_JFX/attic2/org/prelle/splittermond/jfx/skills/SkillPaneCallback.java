/**
 * 
 */
package org.prelle.splittermond.jfx.skills;

import org.prelle.splimo.Skill;

/**
 * @author prelle
 *
 */
public interface SkillPaneCallback {

	public void showAndWaitMasterships(Skill skill);
}
