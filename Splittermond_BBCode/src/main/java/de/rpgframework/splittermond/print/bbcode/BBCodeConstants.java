package de.rpgframework.splittermond.print.bbcode;

/**
 * @author frank.buettner
 *
 */
final class BBCodeConstants {
	/**
	 * If a BBCode has an option (e.g. the color tag) then this string must be
	 * replaced by a chosen option.
	 */
	public static final String OPTION_STRING = "OPTION";
	
	private BBCodeConstants() {
		// don't initialize a util class
	}
}
