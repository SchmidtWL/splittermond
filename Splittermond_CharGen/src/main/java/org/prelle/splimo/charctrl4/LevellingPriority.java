/**
 * 
 */
package org.prelle.splimo.charctrl4;

/**
 * @author prelle
 *
 */
public enum LevellingPriority {
	
	/*
	 * Creation: 2 Points for attributes
	 * HG 1: 
	 */
	NOT_TOTALLY_NEGLECTED,
	
	BEING_ABLE_TO_DO_THAT_TOO,
	
	EXPERT,
	
	MASTER,
	
	LEGENDARY,

}
