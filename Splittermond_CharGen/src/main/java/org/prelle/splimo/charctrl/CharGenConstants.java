/**
 * 
 */
package org.prelle.splimo.charctrl;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface CharGenConstants {

	public final static ResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/splimo/chargen/i18n/splittermond/chargen");
	
}
