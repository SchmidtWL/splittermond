package de.rpgframework.splittermond.print.json.model;

import org.prelle.splimo.items.Availability;
import org.prelle.splimo.items.Complexity;

public class JSONItem {
    public String name;
    public int count;
    public boolean relic;
    public boolean personalized;
    public int load;
    public int price;
    public String availability;
    public int quality;
    public String complexity;
    public int durability;
}
